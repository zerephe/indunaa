package com.zerephe.indunaa;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class OrdersRecyclerView extends AppCompatActivity {

    public List<OrderItems> orders;
    private RecyclerView mRecyclerView;

    private FirebaseFirestore database;

    public ImageButton ordersBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_recycler_view);

        database = FirebaseFirestore.getInstance();

        ordersBackButton = findViewById(R.id.orders_back_button);
        ordersBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        orders = new ArrayList<>();

        getDatabase();

        mRecyclerView = findViewById(R.id.orders_recycler_view);

    }

    public void getDatabase(){

        database.collection("books").orderBy("timestamp");

        database.collection("books").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        OrderItems order = document.toObject(OrderItems.class);
                        orders.add(order);
                        setRecyclerView();
                    }
                } else {
                    Log.d("Error: ", "Error getting documents: ", task.getException());
                }

            }
        });
    }

    public void setRecyclerView(){

        OrdersRecyclerViewAdapter ordersRecyclerViewAdapter = new OrdersRecyclerViewAdapter(this, orders);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(ordersRecyclerViewAdapter);

    }

}
