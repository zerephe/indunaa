package com.zerephe.indunaa;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainMap extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest mLocationRequest;

    private static FirebaseAuth mAuth;
    private FirebaseFirestore database;

    public ImageButton signOutButton;
    public ImageButton orderListButton;
    public ImageButton currentLocButton;
    public ImageButton mapPointerA, mapPointerB;

    private NestedScrollView nestedScrollView2;
    private LinearLayout phoneNumberLayout;
    private BottomSheetBehavior mBottomSheetBehaviour, mBottomSheetOrderCompletionBehaviour;
    private Button makeOrderButton, cancelOrderButton;
    private TextView orderIdField, phoneNumberField, roadInfoField;
    private EditText textFieldPointA;
    private EditText textFieldPointB;
    private EditText commentText, cost;
    private Switch drivePassSwitch;
    private ArrayList<LatLng> MarkerPoints;

    private Dialog orderDialog;

    private String orderId = "";
    private String address1;
    private String address2;
    private String state;
    private String zipcode;
    private String country;
    private List<Address> geocodeMatches = null;


    public int toDp(int dp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
        checkState();

        setContentView(R.layout.activity_main_map);

        //init location client
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        View nestedScrollView = findViewById(R.id.nestedScrollView);
        mBottomSheetBehaviour = BottomSheetBehavior.from(nestedScrollView);
        mBottomSheetBehaviour.setPeekHeight(toDp(90));
        mBottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING: {

                        break;
                    }
                    case BottomSheetBehavior.STATE_SETTLING: {

                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {

                        break;
                    }
                    case BottomSheetBehavior.STATE_COLLAPSED: {

                        break;
                    }
                    case BottomSheetBehavior.STATE_HIDDEN: {

                        break;
                    }
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        nestedScrollView2 = findViewById(R.id.nestedScrollView2);
        mBottomSheetOrderCompletionBehaviour = BottomSheetBehavior.from(nestedScrollView2);
        mBottomSheetOrderCompletionBehaviour.setPeekHeight(0);

        phoneNumberLayout = findViewById(R.id.phone_num_L_layout);

        //order completion fields
        cancelOrderButton = findViewById(R.id.cancel_order_button);
        cancelOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               cancelOrder(orderId);

            }
        });

        orderIdField = findViewById(R.id.order_id_field);
        phoneNumberField = findViewById(R.id.phone_number_field);
        roadInfoField = findViewById(R.id.road_info_field);

        currentLocButton = findViewById(R.id.current_loc_button);

        drivePassSwitch = findViewById(R.id.drive_pass_switch);

        textFieldPointA = findViewById(R.id.text_field_pointA);
        textFieldPointA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        textFieldPointB = findViewById(R.id.text_field_pointB);
        commentText = findViewById(R.id.comment_text);
        cost = findViewById(R.id.suggested_price);

        signOutButton = findViewById(R.id.signOut_button);
        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                logOut();
            }
        });

        orderListButton = findViewById(R.id.order_list_button);

        orderListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toOrderList = new Intent(MainMap.this, OrdersRecyclerView.class);
                toOrderList.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(toOrderList);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });


        makeOrderButton = findViewById(R.id.make_order_button);
        makeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               orderId = database.collection("books").document().getId();

                OrderItems order = new OrderItems(
                        orderId,
                        mAuth.getUid(),
                        drivePassSwitch.isChecked(),
                        cost.getText().toString(),
                        commentText.getText().toString(),
                        textFieldPointA.getText().toString(),
                        textFieldPointB.getText().toString(),
                        "12312312",
                        "");

                database.collection("books").document(orderId).set(order).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

//                        String roadInfo = textFieldPointA.getText().toString().trim() + " → " + textFieldPointB.getText().toString().trim();
                        database.collection("users").document(mAuth.getUid()).update("order", orderId);

                    }
                });

            }
        });

        MarkerPoints = new ArrayList<>();
        
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void makeOrder(String roadInfo, final String orderId){

        database.collection("books").document(orderId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot,
                                @javax.annotation.Nullable FirebaseFirestoreException e) {

                if (e != null) {
                    Log.w("Error", "Listen failed.", e);
                    return;
                }

                if (documentSnapshot != null && documentSnapshot.exists()) {
                    if(!documentSnapshot.get("takenBy").toString().equals("") && !documentSnapshot.get("takenBy").toString().equals(mAuth.getUid())){

                        database.collection("users").document(documentSnapshot.get("takenBy").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                database.collection("users").document(documentSnapshot.get("userId").toString()).update("order", orderId);
                                Users user = documentSnapshot.toObject(Users.class);
                                phoneNumberField.setText(user.getPhoneNumber());
                                phoneNumberLayout.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                    else if(documentSnapshot.get("takenBy").toString().equals(mAuth.getUid())){

                        database.collection("users").document(documentSnapshot.get("userId").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                Users user = documentSnapshot.toObject(Users.class);
                                phoneNumberField.setText(user.getPhoneNumber());
                                phoneNumberLayout.setVisibility(View.VISIBLE);
                            }
                        });

                    }

                } else {
                    Log.d("Error", "Current data: null");
                }

            }
        });

        roadInfoField.setText(roadInfo);
        orderIdField.setText(orderId);
        phoneNumberField.setText("");

        mBottomSheetOrderCompletionBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBottomSheetOrderCompletionBehaviour.setPeekHeight(toDp(90));

        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);


    }

    public void cancelOrder(String orderId){

        //resetting fields
        cost.setText("");
        textFieldPointA.setText("");
        textFieldPointB.setText("");
        drivePassSwitch.setChecked(false);
        commentText.setText("");



        database.collection("books").document(orderId).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                database.collection("users").document(mAuth.getUid()).update("order", "").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mBottomSheetOrderCompletionBehaviour.setPeekHeight(0);
                        mBottomSheetOrderCompletionBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });
            }
        });

    }

    int PLACE_PICKER_REQUEST = 1;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {

                // Already two locations
                if (MarkerPoints.size() > 1) {
                    MarkerPoints.clear();
                    mMap.clear();
                }

                // Adding new item to the ArrayList
                MarkerPoints.add(point);

                // Creating MarkerOptions
                MarkerOptions options = new MarkerOptions();

                // Setting the position of the marker
                options.position(point);

                /**
                 * For the start location, the color of marker is GREEN and
                 * for the end location, the color of marker is RED.
                 */


                if (MarkerPoints.size() == 1) {

                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

                    Location tempLoc = new Location("Null");
                    tempLoc.setLatitude(MarkerPoints.get(0).latitude);
                    tempLoc.setLongitude(MarkerPoints.get(0).longitude);

                    setLocation(tempLoc, textFieldPointA);

                }
                else if (MarkerPoints.size() == 2) {

                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                    Location tempLoc = new Location("Null");
                    tempLoc.setLatitude(MarkerPoints.get(1).latitude);
                    tempLoc.setLongitude(MarkerPoints.get(1).longitude);

                    setLocation(tempLoc, textFieldPointB);

                }


                // Add new marker to the Google Map Android API V2
                mMap.addMarker(options);

                // Checks, whether start and end locations are captured
                if (MarkerPoints.size() >= 2) {
                    LatLng origin = MarkerPoints.get(0);
                    LatLng dest = MarkerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getUrl(origin, dest);
                    Log.d("onMapClick", url.toString());
                    FetchUrl FetchUrl = new FetchUrl();

                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                    //move map camera
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
                }

            }
        });

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            mMap.setOnMyLocationButtonClickListener(this);
            mMap.setOnMyLocationClickListener(this);
        }

        createLocationRequest();

        currentLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocation();
            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                            MarkerPoints.add(loc);

                        }
                    }
                });

    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                Log.d("onPostExecute",points.toString());
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);



            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                mMap.addPolyline(lineOptions);
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    //checking if user is logged in
    public void checkState() {

        if (mAuth.getCurrentUser() != null) {
            //pass

            database.collection("users").document(mAuth.getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {

                    if(!documentSnapshot.get("order").toString().equals("")){

                        database.collection("books").document(documentSnapshot.get("order").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                String roadInfo = documentSnapshot.get("pointA").toString().trim() + " → " + documentSnapshot.get("pointB").toString().trim();
                                orderId = documentSnapshot.get("bookId").toString();

                                makeOrder(roadInfo, documentSnapshot.get("bookId").toString());

                            }
                        });
                    }

                }
            });

            database.collection("users").document(mAuth.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {

                    if(!documentSnapshot.get("order").toString().equals("")){

                        database.collection("books").document(documentSnapshot.get("order").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                String roadInfo = documentSnapshot.get("pointA").toString().trim() + " → " + documentSnapshot.get("pointB").toString().trim();
                                orderId = documentSnapshot.get("bookId").toString();

                                makeOrder(roadInfo, documentSnapshot.get("bookId").toString());

                            }
                        });
                    }
                }
            });


        } else {
            logOut();
        }

    }

    public void logOut() {
        Intent toPhoneVerification = new Intent(MainMap.this, PhoneVerification.class);
        toPhoneVerification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        startActivity(toPhoneVerification);
        overridePendingTransition(0, 0);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void setLocation(Location location, EditText textFieldPoint){

        Geocoder geocoder;
        List<Address> addresses;
        String sPlace = "";
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getAddressLine(1);
            String country = addresses.get(0).getAddressLine(2);

            String[] splitAddress = address.split(",");
            sPlace = splitAddress[0] + "\n";
            if(city != null && !city.isEmpty()) {
                String[] splitCity = city.split(",");
                sPlace += splitCity[0];
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        textFieldPoint.setText(sPlace);

    }

    public void currentLocation(){

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            setLocation(location, textFieldPointA);

                            // Add a marker in Sydney and move the camera
                            LatLng bishkek = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(bishkek));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

                        }
                    }
                });
    }

    @Override
    public void onLocationChanged(Location location) {

        setLocation(location, textFieldPointA);

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        currentLocation();

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }
}
