package com.zerephe.indunaa;

public class OrderItems {

    private String bookId, takenBy;
    private String cost;
    private boolean bookType;
    private String comment;
    private String pointA, pointB;
    private Double degreeA, degreeB;
    private String timestamp;
    private String userId;

    public OrderItems(){}

    public OrderItems(String bookId, String userId, boolean bookType, String cost, String comment, String pointA, String pointB, String timestamp, String takenBy ){
        this.bookId = bookId;
        this.cost = cost;
        this.bookType = bookType;
        this.userId = userId;
        this.comment = comment;
        this.pointA = pointA;
        this.pointB = pointB;
        this.timestamp = timestamp;
        this.takenBy = takenBy;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public boolean getBookType() {
        return bookType;
    }

    public void setBookType(boolean bookType) {
        this.bookType = bookType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPointA() {
        return pointA;
    }

    public void setPointA(String pointA) {
        this.pointA = pointA;
    }

    public String getPointB() {
        return pointB;
    }

    public void setPointB(String pointB) {
        this.pointB = pointB;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public Double getDegreeA() {
        return degreeA;
    }

    public void setDegreeA(Double degreeA) {
        this.degreeA = degreeA;
    }

    public Double getDegreeB() {
        return degreeB;
    }

    public void setDegreeB(Double degreeB) {
        this.degreeB = degreeB;
    }

    public String getTakenBy() {
        return takenBy;
    }

    public void setTakenBy(String takenBy) {
        this.takenBy = takenBy;
    }
}
