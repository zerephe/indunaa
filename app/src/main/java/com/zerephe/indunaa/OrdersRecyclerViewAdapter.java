package com.zerephe.indunaa;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

public class OrdersRecyclerViewAdapter extends RecyclerView.Adapter<OrdersRecyclerViewAdapter.OrdersViewHolder> {


    public List<OrderItems> orders;
    public Context mContext;

    private FirebaseAuth mAuth;
    private FirebaseFirestore database;

    public OrdersRecyclerViewAdapter(Context mContext, List<OrderItems> orders){
        this.mContext = mContext;
        this.orders = orders;

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();
    }


    @NonNull
    @Override
    public OrdersRecyclerViewAdapter.OrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order, viewGroup, false);
        final OrdersViewHolder ordersViewHolder = new OrdersViewHolder(view);

        return ordersViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersRecyclerViewAdapter.OrdersViewHolder ordersViewHolder, int i) {

        final String orderId = orders.get(i).getBookId();

        ordersViewHolder.orderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.collection("books").document(orderId).update("takenBy", mAuth.getUid());

                Intent toMain = new Intent(mContext, MainMap.class);
                toMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(toMain);
            }
        });

        String roadInfo;

        Timestamp ts = new Timestamp(Integer.parseInt(orders.get(i).getTimestamp()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        roadInfo = orders.get(i).getPointA().trim() + " → " + orders.get(i).getPointB().trim();

        ordersViewHolder.roadTextView.setText(roadInfo);
        ordersViewHolder.cost.setText(orders.get(i).getCost().trim() + " som");
        ordersViewHolder.commentText.setText(orders.get(i).getComment().trim());
        ordersViewHolder.dateItem.setText(formatter.format(ts));

        if(orders.get(i).getBookType()){
            ordersViewHolder.orderLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orderColorGreen));
        }
        else ordersViewHolder.orderLayout.setBackgroundColor(mContext.getResources().getColor(R.color.floralWhite));

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class OrdersViewHolder extends RecyclerView.ViewHolder {

        private TextView roadTextView, dateItem;
        private TextView commentText, cost;
        private RelativeLayout orderLayout;

        public OrdersViewHolder(@NonNull View itemView) {
            super(itemView);

            dateItem = itemView.findViewById(R.id.item_date);
            roadTextView = itemView.findViewById(R.id.item_road_info);
            cost = itemView.findViewById(R.id.item_cost);
            commentText = itemView.findViewById(R.id.item_comment);
            orderLayout = itemView.findViewById(R.id.order_relative_main);

        }
    }
}
