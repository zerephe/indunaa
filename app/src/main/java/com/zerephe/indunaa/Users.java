package com.zerephe.indunaa;

import android.widget.EditText;

public class Users {

    private String userId, phoneNumber, order;
    private boolean userType;

    public Users(){}

    public Users(String userId, String phoneNumber, boolean userType, String order){

        this.userId = userId;
        this.phoneNumber = phoneNumber;
        this.userType = userType;
        this.order = order;

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isUserType() {
        return userType;
    }

    public void setUserType(boolean userType) {
        this.userType = userType;
    }

    public String getOrder() {
        return order;
    }
}
